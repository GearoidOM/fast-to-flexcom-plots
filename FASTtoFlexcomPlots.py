import argparse
from pathlib import Path
import csv
from csv import DictReader
import xml.etree.ElementTree as ET
from xml.dom import minidom
import re
import os


def main(args):
    print("Processing Plots....")
    print(f"Input File: {args.input}")
    print(f"Input Channels: {args.channels}")
    print(f"Output Directory: {args.output}")

    try:
        file_path = Path(args.input).resolve(strict=True)
    except FileNotFoundError as fnferr:
        print(f"Error: {args.input} not found, {fnferr}")
    else:

        if not args.output: 
            output_dir = os.getcwd()
        else:
            output_dir = Path(args.output).resolve(strict=True)

        try:
            with open(file_path,'r') as in_file:
                in_data = in_file.readlines()
                for index, line in enumerate(in_data):
                    if line.find('Time') >= 0:
                        column_index = index
                        columns = line.split()
                        break
            
            if args.channels:
                plot_channels = args.channels.split()
            else:
                plot_channels = columns

            csv_header = in_data[column_index] 

            csv_fieldnames = [fn.strip() for fn in csv_header.split('\t')]

            unit_row = in_data[column_index+1]

            unit_names = [re.sub(r'[\(\)\s+]','',un) for un in unit_row.split('\t')]

            unit_dict = dict(zip(csv_fieldnames,unit_names))

            
            for column in plot_channels:
                if column == 'Time':
                    continue

                tree = ET.Element('Plot2')

                csv_dict = DictReader(in_data[column_index+2:],fieldnames=csv_fieldnames,delimiter='\t')
                
                plot_set = generate_header(tree,column,unit_dict[column])

                for row in csv_dict:
                    ET.SubElement(plot_set,'PlotPoint',Tool=" ",X=row['Time'],Y=row[column])

                xmlstr = minidom.parseString(ET.tostring(tree)).toprettyxml(indent="   ")

                plot_file = os.path.join(output_dir, f"FAST.{column}.mplt")
                with open(plot_file,'w+') as pfile:
                    pfile.write(xmlstr)
                
                tree.clear()
                
        except OSError as err:
            print(f"Could not open file: {args.input}, {err}")

def generate_header(tree,column,unit):
    header = ET.SubElement(tree,'HeaderInfo')
    header.set('Product',"Flexcom")
    header.set('ProductVersion',"8.10")
    ET.SubElement(header,'Titles',ProjectName="FASTtoFlexcom",GraphName=f"{column} Plot")
    plot = ET.SubElement(tree,'Graph')
    axes = ET.SubElement(plot,'Axes')
    ET.SubElement(axes,'XAxis',Type="Time", Title="Time", BaseUnit="s", Units="s", ID="0", Format="Linear", ViewMin="-.389500E+01", ViewMax="0.127995E+03")
    #ET.SubElement(axes,'YAxis',Type="Force", Title=f"{column}", BaseUnit=f"{unit}", Units=f"{unit}", ID="0", Format="Linear", ViewMin="0.380843E+02", ViewMax="0.224520E+03")
    ET.SubElement(axes,'YAxis',Type="Force", Title=f"{column}", BaseUnit="", Units="", ID="0", Format="Linear", ViewMin="0.380843E+02", ViewMax="0.224520E+03")
    plot_set = ET.SubElement(plot,'Set')
    plot_set.set('Name',f"Time vs {column}")
    plot_set.set('XAxisID',"0")
    plot_set.set('YAxisID',"0")
    plot_set.set('XScale',"0.100000E+01")
    plot_set.set('YScale',"0.100000E+01")
    plot_set.set('Type1',"4")
    plot_set.set('Type2',"1")
    plot_set.set('Type4',"1")
    return plot_set


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', help='Path to FAST output file containing results in text format')
    parser.add_argument('--channels', help='FAST channel names to plot from *.out file')
    parser.add_argument('--output', help='Path to folder to write plots to')
    args = parser.parse_args()
    main(args)
